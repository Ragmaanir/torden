describe Torden::ProgressBar do
  subject { described_class.new(length: 6) }
  it 'play the default animation' do
    expect(subject.to_s).to eq('[    ]')
    subject.update(0.25)
    expect(subject.to_s).to eq('[#   ]')
    subject.update(0.5)
    expect(subject.to_s).to eq('[##  ]')
    subject.update(0.75)
    expect(subject.to_s).to eq('[### ]')
    subject.update(1.0)
    expect(subject.to_s).to eq('[####]')
  end
end
