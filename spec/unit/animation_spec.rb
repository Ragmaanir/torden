describe Torden::Animation do
  subject { described_class.new }
  it 'play the default animation' do
    expect(subject.to_s).to eq('-')
    subject.update
    expect(subject.to_s).to eq('\\')
    subject.update
    expect(subject.to_s).to eq('|')
    subject.update
    expect(subject.to_s).to eq('/')
  end
end
