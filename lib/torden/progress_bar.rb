module Torden
  class ProgressBar

    attr_reader :progress, :inner_length, :space_char, :progress_char

    def initialize(length: 100, space_char: ' ', progress_char: '#')
      @length = length
      @inner_length = @length - 2
      raise ArgumentError if inner_length <= 0
      raise ArgumentError if space_char.length != 1
      raise ArgumentError if progress_char.length != 1
      @space_char = space_char
      @progress_char = progress_char
      @progress = 0
    end

    def update(progress)
      raise unless (0.0..1.0).cover?(progress)
      @progress = progress
      to_s
    end

    def to_s
      "[#{bar}#{space}]"
    end

  private

    def space
      #' ' * (inner_length - bar.length)
      space_char * (inner_length * (1.0 - progress))
    end

    def bar
      progress_char * (@progress*@inner_length).to_i
    end

  end
end
