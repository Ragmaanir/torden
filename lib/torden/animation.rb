module Torden
  class Animation
    SPINNER = %w{- \\ | /}

    def initialize(anim: SPINNER)
      @anim = anim
      @i = 0
    end

    def update
      @i += 1
    end

    def to_s
      @anim[@i % @anim.length]
    end
  end
end
